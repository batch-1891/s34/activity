const express = require('express')

const app = express()

const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended : true}))

app.get('/home', (request, response) => {
	response.send('Hello World!')
})

app.get('/users', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})

app.delete('/delete-user', (request, response) => {
	response.send(`Deleted`)
})


app.listen(port, () => console.log(`Server is running at port ${port}`))










